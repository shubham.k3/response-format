const createStatusCodeError = function (statusCode, message, body) {
    return Object.assign(new Error(), {
        statusCode,
        message,
        body,
    });
};

const badRequestError = function (msg) {
    return createStatusCodeError(422, msg);
};

const unverifiedError = function (message, body) {
    return createStatusCodeError(412, message, body);
};

const forbiddenError = function (msg) {
    return createStatusCodeError(403, msg);
};

const unauthorizedError = function (msg) {
    return createStatusCodeError(401, msg);
};

const notFoundError = function (msg) {
    return createStatusCodeError(404, msg);
};

const gonePage = function (msg) {
    return createStatusCodeError(400, msg);
};

const errorResponse = function (res, message, statusCode) {
    res.statusCode = statusCode;
    return res.json({
        success: false,
        statusCode,
        message,
    });
};

const successResponse = function (res, code, data, message) {
    return res.status(code || 200).json({
        success: true,
        statusCode: code,
        data,
        message,
    });
};

const okResponse = function (res, data, message) {
    res.statusCode = 200;
    if (!message) {
        // eslint-disable-next-line no-param-reassign
        message = '';
    }
    return successResponse(res, 200, data, message);
};

const successData = function (message, data = '') {
    return { statusCode: 200, message, body: data };
};

const createdResponse = function (res, data, message) {
    return successResponse(res, 201, data, message);
};

const noContentResponse = function (res, message) {
    return successResponse(res, 204, {}, message);
};
module.exports = {
    badRequestError,
    okResponse,
    errorResponse,
    unverifiedError,
    forbiddenError,
    noContentResponse,
    createdResponse,
    notFoundError,
    unauthorizedError,
    gonePage,
    successData,
};
